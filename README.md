# file_sync
自动化小工具。监控指定位置的文件，如果文件改动便通过git进行提交。

## 安装
### 环境安装

* Install [git](https://git-scm.com/downloads)
* Install [Python](https://www.python.org/downloads/)
* Install [watchdog](http://pythonhosted.org/watchdog/)

in OS X or *nix:

```
pip install watchdog
```

in Windows:

```
python -m pip install watchdog
```

## 使用
> 以使用github进行同步为例。

### 新建需要同步的文件夹仓库
#### 1. 在github仓库创建远程仓库
![new git repository](imgs/new_git.png)

**注意: 请注意创建的git仓库需要能够通过`git`访问得到: 如将该仓库设置为公开，或者添加个人密钥(ssh连接)或者记录个人账号密码(http连接)。**

#### 2. 将自己的配置上传到仓库中
移动到要同步的配置文件所在的文件夹中，输入以下命令初始化仓库。

```bash
git init
```

输入以下命令与远程仓库建立连接。

```bash
git remote add origin [你建立的仓库的url]
```

*[]中的内容是提示，不是真正要输入到命令行中的东西，请替换成提示中说到的东西。*

创建`.gitignore`文件，记录下你**不需要**同步的文件名。再执行以下命令以将文件初始化到远程仓库之中。

```bash
git add -A
git commit -m "First commit."
git push -u origin master
```

#### 3. 初始化**file_list.txt**文件以指定要监控的配置文件
在上述文件夹中手动新建一个文本文件，命名为`file_list.txt`(不要忘了将这个文件也加到`.gitignore`中)，在其中加入要监控的配置文件的**文件名+绝对路径**，如下所示。
 	
 ```planitext
/Users/Tim/.zshrc
/Users/Tim/Library/Application Support/Sublime Text 2/Packages/User/Default (OSX).sublime-keymap
/Users/Tim/Library/Application Support/Sublime Text 2/Packages/User/Preferences.sublime-settings
 ```

**注意: 每个行对应一个文件，且请使用绝对路径**

### 4. 测试运行
在本项目下打开命令行窗口，输入以下命令:

```bash
python file_sync.py -r [要监控的文件夹] [-b [要同步的目标分支，默认为master分支]]
```

待程序运行后，可以尝试修改一个被监控中的文件。

待修改并保存后，看到命令行中输出如下数据便表示成功运行。

![python run log](imgs/logs.png)

此时登录远程仓库，可以看到远程仓库中对应的文件也发生了更新，表明程序测试成功。

![success](imgs/check.png)

### 5. 将脚本加入到开启启动中
> 该章节下的方法并非唯一(甚至不一定有效 :) )，如果无法使用，可以在网上直接搜索相关方法即可。

* OS X
抱歉，本人没有苹果系电脑 :( ，	具体使用方法请自行百度。
* Windows
这里提供一个最基础的方法:
1. 将上述命令中的`python`改为`pythonw`、将`file_sync.py`改为该文件所在的绝对路径后，写入一个bat预处理文件(注意，如果你使用的是python虚拟环境或者anaconda，请将上述的`pythonw`命令直接更换为对应`pythonw.exe`的绝对路径，以保证需要库的存在)。
2. 按下`win+r`输入`shell:startup`，将上述的bat文件剪切到弹出的文件夹中，重启即可。
* *nix
搜索你的linux发行版本，并找到你对应版本的“自动开机运行脚本”方法，将以下命令写入对应开机自启脚本中即可。

```bash
nohup python [file_sync.py的绝对路径] -r [要监控的文件夹] [-b [要同步的目标分支，默认为master分支]] &
```

*注意: 这里的python命令同样需要注意虚拟环境或者anaconda的问题。*
